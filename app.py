import cv2
from keras.models import load_model
from keras_preprocessing.image import load_img, img_to_array
import numpy as np

model=load_model('face_mask_detection_model.h5')
img_width , img_height = 200 , 200
#thư viện khuôn mặt trên opencv để nhận diện webcam
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
cap = cv2.VideoCapture(0)
img_count_full = 0
font = cv2.FONT_HERSHEY_SIMPLEX
org = (1,1) #org: Nó là tọa độ của góc dưới bên trái của chuỗi văn bản trong hình ảnh( giá trị tọa độ X , giá trị tọa độ Y ).
class_label = ' '
fontScale = 1
color = (255,0,0)
thickness=2

while True:
    img_count_full+=1
    # res là một biến boolean trả về true nếu khung có sẵn
    res , frame = cap.read() # đọc ảnh lấy ra từ cam - clr_img
    if res==False:
        break

    gray_img = cv2.cvtColor(frame , cv2.COLOR_BGR2GRAY)
    # hàm tìm khuôn mặt detectorMultiScale (hình ảnh,scaleFactor-kích thước hình ảnh được giảm bao nhiêu ở mỗi tỷ lệ hình ảnh, minNeighbors) 
    # giảm kích thước xuống 3%, chúng tôi tăng cơ hội tìm thấy kích thước phù hợp với mô hình để phát hiện
    # minNeighbors : Tham số chỉ định số lượng hàng xóm mà mỗi hình chữ nhật ứng viên phải có để giữ lại nó
    faces = face_cascade.detectMultiScale(gray_img , 1.3, 5)
    
    img_cnt = 0
    for (x,y,w,h) in faces:
        org = (x-10, y-10)
        img_cnt +=1
        #cắt nhỏ ảnh
        color_face = frame[y:y+h , x:x+w] 
        #lưu ảnh 
        cv2.imwrite('input_faces/faces/%d%dface.jpg'%(img_count_full , img_cnt) , color_face)
        img = load_img('input_faces/faces/%d%dface.jpg'%(img_count_full , img_cnt) , target_size = (img_width , img_height))
        #Chuyển đổi một phiên bản Hình ảnh PIL thành một mảng Numpy.
        img = img_to_array(img)
        # tạo hàm mở rộngexpand_dims theo trục 0, bạn sẽ có hình dạng (1, 224, 224, 3) axis : Định vị nơi trục mới sẽ được chèn
        img = np.expand_dims(img,axis=0)
        #trả về các nhãn của dữ liệu được truyền dưới dạng đối số dựa trên dữ liệu đã học hoặc được đào tạo thu được từ mô hình.
        pred_p = model.predict(img)
        #đối số của giá trị cực đại
        pred = np.argmax(pred_p)
        if pred == 0:
            print('User with mask = ',pred_p[0][0])
            class_label = 'Mask'
            color = (255,0,0)
            #lưu hình ảnh vào tệp
            cv2.imwrite('input_faces/with_mask/%d%dface.jpg'%(img_count_full , img_cnt) , color_face)
            
        else:
            print('User without mask = ',pred_p[0][1])
            class_label = 'No Mask'
            color = (0,255,0)
            cv2.imwrite('input_faces/without_mask/%d%dface.jpg'%(img_count_full , img_cnt) , color_face)
             # vẽ khung vuông : rectangle (hình ảnh, start_point, end_point, màu sắc, độ dày)
        cv2.rectangle(frame , (x,y) , (x+w,y+h) , (0,0,255) , 3)
             # vẽ một chuỗi văn bản trên hình ảnh
             #putText (hình ảnh, văn bản, tọa độ, phông chữ, fontScale, màu [, độ dày [, lineType [, bottomLeftOrigin]]])
        cv2.putText(frame , class_label , org , font , fontScale , color , thickness , cv2.LINE_AA)
    cv2.imshow("LIVE FACE MASK DETECTION",frame)
        
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
            
cap.release()
cv2.destroyAllWindows()